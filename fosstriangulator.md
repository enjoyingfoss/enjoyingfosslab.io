---
layout: project.html
pageSlug: fosstriangulator
preview: assets/preview_fosstriangulator.webp
title: FOSStriangulator
logo: assets/logo_color_fosstriangulator.svg
flathubLink: https://flathub.org/apps/org.enjoyingfoss.FOSStriangulator
microsoftStoreLink: https://www.microsoft.com/store/apps/9n6mf3dngr9q
liberapayLink: https://liberapay.com/FOSStriangulator/donate
ghDevelopLink: https://github.com/FOSStriangulator/FOSStriangulator
ghBugLink: https://github.com/FOSStriangulator/FOSStriangulator/issues
contributeBgColor: "bg-fosstriangulator-50"
linkColor: "text-fosstriangulator-800"
metaDescription: "A tool for making triangulated illustrations out of photos"
metaKeywords: "triangulator, triangulation, low-poly, open-source, free software"
---

A tool for making triangulated illustrations out of photos.

- **Offline-first**, data is stored on-device
- Take any photo and make a **low-poly triangulated version** out of it!
- Export to **SVG, PDF, or OBJ**, or just save the **points**
- **Private, no ads, no tracking**