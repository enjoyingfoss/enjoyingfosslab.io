---
layout: project.html
pageSlug: feeel
preview: assets/preview_feeel.webp
title: Feeel
logo: assets/logo_color_feeel.svg
flathubLink: https://flathub.org/apps/com.enjoyingfoss.feeel
fdroidLink: https://f-droid.org/packages/com.enjoyingfoss.feeel/
playStoreLink: https://play.google.com/store/apps/details?id=com.enjoyingfoss.feeel
liberapayLink: https://liberapay.com/Feeel/donate
wgerWikiLink: https://wger.de/en-gb/exercise/overview/
lowPolyGuideLink: https://gitlab.com/enjoyingfoss/feeel/-/wikis/Processing-photos
weblateLink: https://hosted.weblate.org/projects/feeel/
glDevelopLink: https://gitlab.com/enjoyingfoss/feeel
glBugLink: https://gitlab.com/enjoyingfoss/feeel/-/issues
matrixLink: https://matrix.to/#/#feeel:matrix.org
contributeBgColor: "bg-feeel-50"
linkColor: "text-feeel-700"
metaDescription: "A fitness app that respects your privacy"
metaKeywords: "fitness, workout, exercise, open-source, free software, android"
---

A fitness app that respects your privacy.

- **Offline-first**, data is stored on-device
- Play **guided workouts** (text-to-speech or sound)
- Create **custom workouts**
- **50+ bundled exercises**, with the wger exercise wiki soon to be integrated
- **Private, no ads, no tracking**