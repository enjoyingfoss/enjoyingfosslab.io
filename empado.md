---
layout: project.html
pageSlug: empado
preview: assets/preview_empado.webp
title: Empado
logo: assets/logo_color_empado.svg
playStoreLink: https://play.google.com/apps/testing/com.enjoyingfoss.empado
liberapayLink: https://liberapay.com/Empado/donate
weblateLink: https://hosted.weblate.org/projects/empado/ui/
glDevelopLink: https://gitlab.com/enjoyingfoss/empado
glBugLink: https://gitlab.com/enjoyingfoss/empado/-/issues
matrixLink: https://matrix.to/#/#empado:matrix.org
contributeBgColor: "bg-empado-50"
linkColor: "text-empado-700"
metaDescription: "An emotion and need journal that helps you find life strategies"
metaKeywords: "mood journal, emotion journal, nonviolent communication, nvc, open-source, free software, android"
---

An emotion and need diary that helps you find strategies to solve everyday problems.

- **Offline-first**, data is stored on-device
- Get an overview of your **journal entries**
- Brainstorm and manage **life strategies** that meet your needs
- **Private, no ads, no tracking**

Empado is currently in an open beta.