module.exports = {
    content: [
        "./**/*.{html,js}",
        "./_layouts/**/*.{html,js}",
        "./_includes/**/*.{html,js}"
    ],
    theme: {
        container: {
            center: true,
            padding: '1rem',
        },
        extend: {
            colors: {
                feeel: {
                    50: '#eef9ff',
                    100: '#d8f1ff',
                    200: '#b9e6ff',
                    300: '#89d8ff',
                    400: '#52c2ff',
                    500: '#2aa2ff',
                    600: '#1384fd',
                    700: '#0b65db',
                    800: '#1156bc',
                    900: '#144b94',
                    950: '#112e5a',
                },
                parlera: {
                    50: '#fdf3f3',
                    100: '#fce4e5',
                    200: '#fbcdcf',
                    300: '#f7aaae',
                    400: '#f0797f',
                    500: '#e33e46',
                    600: '#d23038',
                    700: '#b0252c',
                    800: '#922227',
                    900: '#792327',
                    950: '#420d10',
                },
                empado: {
                    50: '#fdf9ed',
                    100: '#f9eecc',
                    200: '#f3dd94',
                    300: '#edc65c',
                    400: '#e8b137',
                    500: '#e09220',
                    600: '#c67019',
                    700: '#a55018',
                    800: '#863f1a',
                    900: '#6f3518',
                    950: '#3f1a09',
                },
                fosstriangulator: {
                    50: '#eaf7fb',
                    100: '#d1eef5',
                    200: '#a7e2ec',
                    300: '#68d3df',
                    400: '#2dc2d2',
                    500: '#2db7d2',
                    600: '#259ab1',
                    700: '#1e808f',
                    800: '#01828e',
                    900: '#066d74',
                    950: '#004e52',
                },

            }
        },
    },
    safelist: [
        'hover:text-feeel-700',
        'hover:text-parlera-600',
        'hover:text-empado-700',
        'hover:text-fosstriangulator-800',
        {
            pattern: /bg-(feeel|parlera|empado|fosstriangulator)-50/,
        },
    ],
    plugins: [],
}