---
layout: project.html
pageSlug: parlera
preview: assets/preview_parlera.webp
title: Parlera
logo: assets/logo_color_parlera.svg
flathubLink: https://flathub.org/apps/com.enjoyingfoss.Parlera
fdroidLink: https://f-droid.org/packages/com.enjoyingfoss.parlera/
playStoreLink: https://play.google.com/store/apps/details?id=com.enjoyingfoss.parlera
liberapayLink: https://liberapay.com/Parlera/donate
weblateLink: https://hosted.weblate.org/projects/parlera/
glDevelopLink: https://gitlab.com/enjoyingfoss/parlera
glBugLink: https://gitlab.com/enjoyingfoss/parlera/-/issues
matrixLink: https://matrix.to/#/#parlera:matrix.org
categoriesLink: https://gitlab.com/enjoyingfoss/parlera-categories/-/tree/main/
contributeBgColor: "bg-parlera-50"
linkColor: "text-parlera-600"
metaDescription: "A party game where your friends describe and you guess"
metaKeywords: "charades, party game, word game, open-source, free software, android"
---

A charades party game where your friends describe and you guess. Or vice versa.

- **Offline-first**, data is stored on-device
- Show your friends the screen, let them describe the words that come up, and **guess**!
- Create **custom categories** with your own words
- **15+ languages** contributed by the community
- **Private, no ads, no tracking**