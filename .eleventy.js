module.exports = function(eleventyConfig) {
  eleventyConfig.setUseGitIgnore(false);
  eleventyConfig.addPassthroughCopy("assets");
  eleventyConfig.addPassthroughCopy(".well-known");
  eleventyConfig.addPassthroughCopy({
    "styles/tailwind.out.css": "tailwind.css",
  });
  return {
    dir: {
      output: "_site",
      includes: "_includes",
      layouts: "_layouts"
    },
  };
};
